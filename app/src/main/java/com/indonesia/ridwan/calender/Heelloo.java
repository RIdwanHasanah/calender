package com.indonesia.ridwan.calender;

import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;

public class Heelloo extends DialogFragment {
    private EditText tglA;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.activity_heelloo,container,false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        String tgl=getArguments().getString("tgl");
        tglA=(EditText)view.findViewById(R.id.tgl);
        tglA.setText(tgl);


        return view;
    }
}
